package pl.software21.carapp.app.utils;

import org.junit.Test;
import org.mockito.cglib.core.Constants;

import pl.software21.carapp.activity.TestConstants;

import static org.junit.Assert.*;

/**
 * Created by RavB on 21.06.2017.
 */
public class UtilsTest {

    @Test
    public void getMarkerTitle() throws Exception {
        assertEquals(TestConstants.CAR1_MAKE + " " + TestConstants.CAR1_MODEL_NAME, Utils.getMarkerTitle(TestConstants.getCar1()));
    }

    @Test
    public void getImageUrl() throws Exception {
        String url = "https://prod.drive-now-content.com/fileadmin/user_upload_global/assets/cars/" + TestConstants.CAR1_MODEL_IDENTIFIER + "/" + TestConstants.CAR1_COLOR + "/2x/car.png";
        assertEquals(url, Utils.getImageUrl(TestConstants.getCar1()));
    }

}