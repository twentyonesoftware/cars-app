package pl.software21.carapp.activity.cars.mvp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import pl.software21.carapp.activity.cars.mvp.view.CarsView;
import pl.software21.carapp.app.network.model.Car;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by RavB on 21.06.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class CarsPresenterTest {

    @Mock
    CarsView carsView;
    @Mock
    CarsModel carsModel;

    private CarsPresenter presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new CarsPresenter(carsView, carsModel);
    }

    @Test
    public void onCreate() throws Exception {
        presenter.onCreate();

        verify(carsView, times(1)).hideList();
        verify(carsView, times(1)).hideMapButton();
        verify(carsView, times(1)).showProgress();

        verify(carsModel, times(1)).downloadCars(presenter);
    }

    @Test
    public void onDestroy() throws Exception {
        presenter.onDestroy();

        verify(carsModel, times(1)).cancelDownloadingCars();
    }

    @Test
    public void onShowMapButtonClicked() throws Exception {
        List<Car> cars = new ArrayList<>();
        when(carsModel.getCars()).thenReturn(cars);

        presenter.onShowMapButtonClicked();

        verify(carsView, times(1)).showMapActivity(cars);
    }

    @Test
    public void onSuccess() throws Exception {
        List<Car> cars = new ArrayList<>();

        presenter.onSuccess(cars);

        verify(carsView, times(1)).hideProgress();
        verify(carsView, times(1)).showList();
        verify(carsView, times(1)).showMapButton();

        verify(carsView, times(1)).setupCarsList(cars);
    }

    @Test
    public void onFailure() throws Exception {
        String failureMsg = "Error downloading cars";

        presenter.onFailure(failureMsg);

        verify(carsView, times(1)).hideProgress();
        verify(carsView, times(1)).showErrorMsg(failureMsg);
    }

}