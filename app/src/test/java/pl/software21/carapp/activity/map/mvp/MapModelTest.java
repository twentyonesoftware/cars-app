package pl.software21.carapp.activity.map.mvp;

import android.content.Intent;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import pl.software21.carapp.activity.TestConstants;
import pl.software21.carapp.activity.map.MapActivity;
import pl.software21.carapp.app.network.model.Car;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by RavB on 21.06.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class MapModelTest {

    private MapModel mapModel;

    @Before
    public void setUp() throws Exception {
        mapModel = new MapModel();
    }

    @Test
    public void getCars() throws Exception {
        List<Car> cars = TestConstants.generateCarList();

        Intent intent = mock(Intent.class);
        when(intent.getParcelableArrayListExtra(MapActivity.CAR_LIST_EXTRA)).thenReturn(TestConstants.generateIntentExtra());

        mapModel.onIntentReceived(intent);

        assertEquals(2, mapModel.getCars().size());
        assertTrue(mapModel.getCars().contains(cars.get(0)));
        assertTrue(mapModel.getCars().contains(cars.get(1)));

    }

    @Test
    public void getCarsWithoutIntent() throws Exception {
        assertEquals(0, mapModel.getCars().size());
    }

}