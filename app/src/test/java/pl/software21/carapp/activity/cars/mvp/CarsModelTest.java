package pl.software21.carapp.activity.cars.mvp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import okhttp3.ResponseBody;
import pl.software21.carapp.activity.TestConstants;
import pl.software21.carapp.app.network.RemoteInterface;
import pl.software21.carapp.app.network.model.Car;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by RavB on 21.06.2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class CarsModelTest {

    @Mock
    RemoteInterface remoteInterface;
    @Mock
    Call<List<Car>> call;
    @Mock
    ResponseBody responseBody;
    @Captor
    ArgumentCaptor<Callback<List<Car>>> argumentCapture;

    private CarsModel carsModel;
    private List<Car> cars;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        carsModel = new CarsModel(remoteInterface);
        cars = TestConstants.generateCarList();
    }

    @Test
    public void downloadCarsSuccess() throws Exception {
        when(remoteInterface.getCars()).thenReturn(call);
        Response<List<Car>> response = Response.success(cars);

        CarsModel.DownloadingListener listener = mock(CarsModel.DownloadingListener.class);

        carsModel.downloadCars(listener);

        verify(call).enqueue(argumentCapture.capture());
        argumentCapture.getValue().onResponse(null, response);

        verify(listener).onSuccess(cars);
    }

    @Test
    public void downloadCarsBadRequest() throws Exception {
        when(remoteInterface.getCars()).thenReturn(call);
        Response<List<Car>> response = Response.error(400, responseBody);

        CarsModel.DownloadingListener listener = mock(CarsModel.DownloadingListener.class);

        carsModel.downloadCars(listener);

        verify(call).enqueue(argumentCapture.capture());
        argumentCapture.getValue().onResponse(null, response);

        verify(listener).onFailure("Error downloading cars");

    }

    @Test
    public void downloadCarsFailure() throws Exception {
        when(remoteInterface.getCars()).thenReturn(call);
        Throwable throwable = new Throwable(new RuntimeException());

        CarsModel.DownloadingListener listener = mock(CarsModel.DownloadingListener.class);

        carsModel.downloadCars(listener);

        verify(call).enqueue(argumentCapture.capture());
        argumentCapture.getValue().onFailure(null, throwable);

        verify(listener).onFailure(throwable.getMessage());
    }
    @Test
    public void cancelDownloadingCars() throws Exception {
        when(remoteInterface.getCars()).thenReturn(call);

        CarsModel.DownloadingListener listener = mock(CarsModel.DownloadingListener.class);

        carsModel.downloadCars(listener);
        carsModel.cancelDownloadingCars();

        verify(call, times(1)).cancel();
    }

}