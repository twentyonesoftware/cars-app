package pl.software21.carapp.activity.map.mvp;

import android.content.Intent;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import pl.software21.carapp.activity.TestConstants;
import pl.software21.carapp.app.network.model.Car;
import pl.software21.carapp.app.utils.Utils;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by RavB on 21.06.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class MapPresenterTest {

    @Mock
    MapView mapView;
    @Mock
    MapModel mapModel;

    private MapPresenter presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new MapPresenter(mapView, mapModel);
    }

    @Test
    public void onCreate() throws Exception {
        Intent intent = mock(Intent.class);

        presenter.onCreate(intent);

        verify(mapModel, times(1)).onIntentReceived(intent);
    }

    @Test
    public void onMapReady() throws Exception {
        List<Car> cars = new ArrayList<>();
        cars.add(TestConstants.getCar1());
        cars.add(TestConstants.getCar2());

        when(mapModel.getCars()).thenReturn(cars);

        presenter.onMapReady();

        verify(mapView).moveCamera(TestConstants.CAR1_LATITUDE, TestConstants.CAR1_LONGITUDE, 12);

        verify(mapView).showMarker(TestConstants.CAR1_LATITUDE, TestConstants.CAR1_LONGITUDE, Utils.getMarkerTitle(TestConstants.getCar1()));
        verify(mapView).showMarker(TestConstants.CAR2_LATITUDE, TestConstants.CAR2_LONGITUDE, Utils.getMarkerTitle(TestConstants.getCar2()));

    }

    @Test
    public void onMapReadyWhenNoCars() throws Exception {
        List<Car> cars = new ArrayList<>();

        when(mapModel.getCars()).thenReturn(cars);

        presenter.onMapReady();

        verify(mapView, never()).moveCamera(TestConstants.CAR1_LATITUDE, TestConstants.CAR1_LONGITUDE, 14);

        verify(mapView, never()).showMarker(TestConstants.CAR1_LATITUDE, TestConstants.CAR1_LONGITUDE, Utils.getMarkerTitle(TestConstants.getCar1()));
        verify(mapView, never()).showMarker(TestConstants.CAR2_LATITUDE, TestConstants.CAR2_LONGITUDE, Utils.getMarkerTitle(TestConstants.getCar2()));

    }

}