package pl.software21.carapp.activity;

import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import pl.software21.carapp.app.network.model.Car;

/**
 * Created by RavB on 21.06.2017.
 */

public class TestConstants {

    public static final String CAR1_ID = "WMWSW31030T222518";
    public static final String CAR1_MODEL_IDENTIFIER = "mini";
    public static final String CAR1_MODEL_NAME = "MINI";
    public static final String CAR1_NAME = "Vanessa";
    public static final String CAR1_MAKE = "BMW";
    public static final String CAR1_GROUP = "MINI";
    public static final String CAR1_COLOR = "midnight_black";
    public static final String CAR1_SERIES = "MINI";
    public static final String CAR1_FUEL_TYPE = "D";
    public static final double CAR1_FUEL_LEVEL = 0.7;
    public static final String CAR1_TRANSMISSION = "M";
    public static final String CAR1_LICENSE_PLATE = "M-VO0259";
    public static final double CAR1_LATITUDE = 48.134557;
    public static final double CAR1_LONGITUDE = 11.576921;
    public static final String CAR1_INNER_CLEANLINESS = "REGULAR";

    public static final String CAR2_ID = "WMWZN31070T054853";
    public static final String CAR2_MODEL_IDENTIFIER = "mini_cabrio";
    public static final String CAR2_MODEL_NAME = "MINI Cabrio";
    public static final String CAR2_NAME = "Florian";
    public static final String CAR2_MAKE = "BMW";
    public static final String CAR2_GROUP = "MINI";
    public static final String CAR2_COLOR = "hot_chocolate";
    public static final String CAR2_SERIES = "Cabrio";
    public static final String CAR2_FUEL_TYPE = "P";
    public static final double CAR2_FUEL_LEVEL = 0.55;
    public static final String CAR2_TRANSMISSION = "M";
    public static final String CAR2_LICENSE_PLATE = "M-I7362";
    public static final double CAR2_LATITUDE = 48.168436;
    public static final double CAR2_LONGITUDE = 11.526889;
    public static final String CAR2_INNER_CLEANLINESS = "CLEAN";

    public static Car getCar1() {
        Car car = new Car();
        car.setId(CAR1_ID);
        car.setModelIdentifier(CAR1_MODEL_IDENTIFIER);
        car.setModelName(CAR1_MODEL_NAME);
        car.setName(CAR1_NAME);
        car.setMake(CAR1_MAKE);
        car.setGroup(CAR1_GROUP);
        car.setColor(CAR1_COLOR);
        car.setSeries(CAR1_SERIES);
        car.setFuelType(CAR1_FUEL_TYPE);
        car.setFuelLevel(CAR1_FUEL_LEVEL);
        car.setTransmission(CAR1_TRANSMISSION);
        car.setLicensePlate(CAR1_LICENSE_PLATE);
        car.setLatitude(CAR1_LATITUDE);
        car.setLongitude(CAR1_LONGITUDE);
        car.setInnerCleanliness(CAR1_INNER_CLEANLINESS);

        return car;
    }

    public static Car getCar2() {
        Car car = new Car();
        car.setId(CAR2_ID);
        car.setModelIdentifier(CAR2_MODEL_IDENTIFIER);
        car.setModelName(CAR2_MODEL_NAME);
        car.setName(CAR2_NAME);
        car.setMake(CAR2_MAKE);
        car.setGroup(CAR2_GROUP);
        car.setColor(CAR2_COLOR);
        car.setSeries(CAR2_SERIES);
        car.setFuelType(CAR2_FUEL_TYPE);
        car.setFuelLevel(CAR2_FUEL_LEVEL);
        car.setTransmission(CAR2_TRANSMISSION);
        car.setLicensePlate(CAR2_LICENSE_PLATE);
        car.setLatitude(CAR2_LATITUDE);
        car.setLongitude(CAR2_LONGITUDE);
        car.setInnerCleanliness(CAR2_INNER_CLEANLINESS);

        return car;
    }

    public static ArrayList<Parcelable> generateIntentExtra() {
        ArrayList<Parcelable> parcelables = new ArrayList<>();

        parcelables.add(getCar1());
        parcelables.add(getCar2());

        return parcelables;
    }

    public static List<Car> generateCarList() {
        List<Car> cars = new ArrayList<>();

        cars.add(getCar1());
        cars.add(getCar2());

        return cars;
    }
}
