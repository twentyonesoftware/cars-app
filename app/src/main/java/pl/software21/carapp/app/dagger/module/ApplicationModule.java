package pl.software21.carapp.app.dagger.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import pl.software21.carapp.app.dagger.ApplicationScope;

/**
 * Created by RavB on 20.06.2017.
 */
@Module
public class ApplicationModule {

    private final Context context;

    public ApplicationModule(Context context) {
        this.context = context.getApplicationContext();
    }

    @ApplicationScope
    @Provides
    public Context context() {
        return context;
    }
}
