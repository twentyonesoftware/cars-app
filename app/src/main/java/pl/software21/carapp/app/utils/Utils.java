package pl.software21.carapp.app.utils;

import pl.software21.carapp.app.network.model.Car;

/**
 * Created by RavB on 21.06.2017.
 */

public class Utils {

    public static String getMarkerTitle(Car car) {
        return car.getMake() + " " + car.getModelName();
    }

    public static String getImageUrl(Car car) {
        String url = "https://prod.drive-now-content.com/fileadmin/user_upload_global/assets/cars/" +
                car.getModelIdentifier() + "/" +
                car.getColor() + "/2x/car.png";

        return url;
    }
}
