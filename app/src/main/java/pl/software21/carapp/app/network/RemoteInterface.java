package pl.software21.carapp.app.network;

import java.util.List;

import pl.software21.carapp.app.network.model.Car;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by RavB on 20.06.2017.
 */

public interface RemoteInterface {

    @GET("cars.json")
    Call<List<Car>> getCars();

}
