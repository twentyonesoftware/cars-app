package pl.software21.carapp.app.dagger.module;

import android.content.Context;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import pl.software21.carapp.app.dagger.ApplicationScope;

/**
 * Created by RavB on 20.06.2017.
 */

@Module(includes = {ApplicationModule.class, NetworkModule.class})
public class PicassoModule {

    @ApplicationScope
    @Provides
    public Picasso picasso(Context context, OkHttp3Downloader okHttp3Downloader) {
        return new Picasso.Builder(context)
                .downloader(okHttp3Downloader)
                .build();
    }

    @ApplicationScope
    @Provides
    public OkHttp3Downloader okHttp3Downloader(OkHttpClient okHttpClient) {
        return new OkHttp3Downloader(okHttpClient);
    }

}
