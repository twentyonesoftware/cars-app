package pl.software21.carapp.app.dagger;

import com.squareup.picasso.Picasso;

import dagger.Component;
import pl.software21.carapp.app.dagger.module.PicassoModule;
import pl.software21.carapp.app.dagger.module.RemoteInterfaceModule;
import pl.software21.carapp.app.network.RemoteInterface;

/**
 * Created by RavB on 20.06.2017.
 */

@ApplicationScope
@Component(modules = {RemoteInterfaceModule.class, PicassoModule.class})
public interface CarApplicationComponent {

    RemoteInterface getRemoteInterface();

    Picasso getPicasso();

}
