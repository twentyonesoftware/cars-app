package pl.software21.carapp.app.dagger.module;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import pl.software21.carapp.app.dagger.ApplicationScope;
import pl.software21.carapp.app.network.RemoteInterface;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RavB on 20.06.2017.
 */

@Module(includes = NetworkModule.class)
public class RemoteInterfaceModule {

    @ApplicationScope
    @Provides
    public RemoteInterface remoteInterface(Retrofit retrofit) {
        return retrofit.create(RemoteInterface.class);
    }

    @ApplicationScope
    @Provides
    public Retrofit retrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .baseUrl("http://www.codetalk.de/")
                .build();
    }

}
