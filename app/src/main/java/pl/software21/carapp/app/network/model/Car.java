package pl.software21.carapp.app.network.model;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by RavB on 20.06.2017.
 */

public class Car implements Parcelable {

    private String id;
    private String modelIdentifier;
    private String modelName;
    private String name;
    private String make;
    private String group;
    private String color;
    private String series;
    private String fuelType;
    private double fuelLevel;
    private String transmission;
    private String licensePlate;
    private double latitude;
    private double longitude;
    private String innerCleanliness;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModelIdentifier() {
        return modelIdentifier;
    }

    public void setModelIdentifier(String modelIdentifier) {
        this.modelIdentifier = modelIdentifier;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public double getFuelLevel() {
        return fuelLevel;
    }

    public void setFuelLevel(double fuelLevel) {
        this.fuelLevel = fuelLevel;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getInnerCleanliness() {
        return innerCleanliness;
    }

    public void setInnerCleanliness(String innerCleanliness) {
        this.innerCleanliness = innerCleanliness;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.modelIdentifier);
        dest.writeString(this.modelName);
        dest.writeString(this.name);
        dest.writeString(this.make);
        dest.writeString(this.group);
        dest.writeString(this.color);
        dest.writeString(this.series);
        dest.writeString(this.fuelType);
        dest.writeDouble(this.fuelLevel);
        dest.writeString(this.transmission);
        dest.writeString(this.licensePlate);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeString(this.innerCleanliness);
    }

    public Car() {
    }

    protected Car(Parcel in) {
        this.id = in.readString();
        this.modelIdentifier = in.readString();
        this.modelName = in.readString();
        this.name = in.readString();
        this.make = in.readString();
        this.group = in.readString();
        this.color = in.readString();
        this.series = in.readString();
        this.fuelType = in.readString();
        this.fuelLevel = in.readDouble();
        this.transmission = in.readString();
        this.licensePlate = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.innerCleanliness = in.readString();
    }

    public static final Creator<Car> CREATOR = new Creator<Car>() {
        @Override
        public Car createFromParcel(Parcel source) {
            return new Car(source);
        }

        @Override
        public Car[] newArray(int size) {
            return new Car[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        return id != null ? id.equals(car.id) : car.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
