package pl.software21.carapp.app;

import android.app.Application;

import pl.software21.carapp.app.dagger.CarApplicationComponent;
import pl.software21.carapp.app.dagger.DaggerCarApplicationComponent;
import pl.software21.carapp.app.dagger.module.ApplicationModule;
import timber.log.Timber;

/**
 * Created by RavB on 20.06.2017.
 */

public class CarApplication extends Application {

    private CarApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        Timber.plant(new Timber.DebugTree());

        component = DaggerCarApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public CarApplicationComponent getComponent() {
        return component;
    }
}
