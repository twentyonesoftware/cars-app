package pl.software21.carapp.activity.map.mvp;

import android.content.Intent;

import java.util.List;

import pl.software21.carapp.app.network.model.Car;
import pl.software21.carapp.app.utils.Utils;

/**
 * Created by RavB on 21.06.2017.
 */

public class MapPresenter {

    private MapView mapView;
    private MapModel mapModel;

    public MapPresenter(MapView mapView, MapModel mapModel) {
        this.mapView = mapView;
        this.mapModel = mapModel;
    }

    public void onCreate(Intent intent) {
        mapModel.onIntentReceived(intent);
    }

    public void onMapReady() {
        List<Car> cars = mapModel.getCars();

        if (cars.size() > 0) {
            mapView.moveCamera(cars.get(0).getLatitude(), cars.get(0).getLongitude(), 12);

            for (Car car : cars) {
                mapView.showMarker(car.getLatitude(), car.getLongitude(), Utils.getMarkerTitle(car));
            }
        }
    }

}
