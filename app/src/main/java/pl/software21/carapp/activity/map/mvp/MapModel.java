package pl.software21.carapp.activity.map.mvp;

import android.content.Intent;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pl.software21.carapp.activity.map.MapActivity;
import pl.software21.carapp.app.network.model.Car;

/**
 * Created by RavB on 21.06.2017.
 */

public class MapModel {

    private Intent intent;

    public void onIntentReceived(Intent intent) {
        this.intent = intent;
    }

    public List<Car> getCars() {
        if (intent == null) {
            return Collections.emptyList();
        }
        return intent.getParcelableArrayListExtra(MapActivity.CAR_LIST_EXTRA);
    }

}
