package pl.software21.carapp.activity.map.dagger;

import dagger.Component;
import pl.software21.carapp.activity.map.MapActivity;
import pl.software21.carapp.app.dagger.CarApplicationComponent;

/**
 * Created by RavB on 21.06.2017.
 */

@MapScope
@Component(modules = MapModule.class, dependencies = CarApplicationComponent.class)
public interface MapComponent {

    void inject(MapActivity activity);

}
