package pl.software21.carapp.activity.cars.mvp.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.software21.carapp.R;
import pl.software21.carapp.app.network.model.Car;
import pl.software21.carapp.app.utils.Utils;

/**
 * Created by RavB on 20.06.2017.
 */

public class CarsListAdapter extends RecyclerView.Adapter<CarsListAdapter.ViewHolder> {

    private Picasso picasso;
    private List<Car> cars;

    public CarsListAdapter(Picasso picasso) {
        this.picasso = picasso;
        this.cars = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cars_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        picasso.load(Utils.getImageUrl(cars.get(position))).into(holder.imageView);
        holder.makeTextView.setText(cars.get(position).getMake());
        holder.nameTextView.setText(cars.get(position).getModelName());
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }

    public void swapCars(List<Car> cars) {
        this.cars.clear();
        this.cars.addAll(cars);
        this.notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cars_list_car_image)
        ImageView imageView;
        @BindView(R.id.cars_list_adapter_make)
        TextView makeTextView;
        @BindView(R.id.cars_list_adapter_name)
        TextView nameTextView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
