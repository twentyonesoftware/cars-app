package pl.software21.carapp.activity.cars.mvp;

import java.util.List;

import pl.software21.carapp.app.network.RemoteInterface;
import pl.software21.carapp.app.network.model.Car;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RavB on 20.06.2017.
 */

public class CarsModel {

    public interface DownloadingListener {

        void onSuccess(List<Car> carList);

        void onFailure(String msg);

    }

    private RemoteInterface remoteInterface;
    protected Call<List<Car>> call;
    private List<Car> cars;

    public CarsModel(RemoteInterface remoteInterface) {
        this.remoteInterface = remoteInterface;
    }

    public void downloadCars(final DownloadingListener downloadingListener) {
        call = remoteInterface.getCars();
        call.enqueue(new Callback<List<Car>>() {
            @Override
            public void onResponse(Call<List<Car>> call, Response<List<Car>> response) {
                if (response.isSuccessful()) {
                    cars = response.body();
                    downloadingListener.onSuccess(cars);
                } else {
                    downloadingListener.onFailure("Error downloading cars");
                }
            }

            @Override
            public void onFailure(Call<List<Car>> call, Throwable t) {
                downloadingListener.onFailure(t.getMessage());
            }
        });

    }

    public void cancelDownloadingCars() {
        if (call != null) {
            call.cancel();
        }
    }

    public List<Car> getCars() {
        return cars;
    }
}
