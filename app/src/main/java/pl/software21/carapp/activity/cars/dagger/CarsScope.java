package pl.software21.carapp.activity.cars.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by RavB on 20.06.2017.
 */

@Scope
@Retention(RetentionPolicy.CLASS)
public @interface CarsScope {
}
