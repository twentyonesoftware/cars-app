package pl.software21.carapp.activity.cars.dagger;

import dagger.Component;
import pl.software21.carapp.activity.cars.CarsActivity;
import pl.software21.carapp.app.dagger.CarApplicationComponent;

/**
 * Created by RavB on 20.06.2017.
 */

@CarsScope
@Component(modules = CarsModule.class, dependencies = CarApplicationComponent.class)
public interface CarsComponent {

    void inject(CarsActivity carsActivity);

}
