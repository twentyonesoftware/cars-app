package pl.software21.carapp.activity.cars;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.software21.carapp.R;
import pl.software21.carapp.activity.cars.dagger.CarsModule;
import pl.software21.carapp.activity.cars.dagger.DaggerCarsComponent;
import pl.software21.carapp.activity.cars.mvp.CarsPresenter;
import pl.software21.carapp.activity.cars.mvp.view.CarsListAdapter;
import pl.software21.carapp.activity.cars.mvp.view.CarsView;
import pl.software21.carapp.activity.map.MapActivity;
import pl.software21.carapp.app.CarApplication;
import pl.software21.carapp.app.network.model.Car;

/**
 * Created by RavB on 20.06.2017.
 */

public class CarsActivity extends AppCompatActivity implements CarsView {

    @Inject
    CarsPresenter carsPresenter;

    @Inject
    CarsListAdapter carsListAdapter;

    @BindView(R.id.cars_view_progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.cars_view_cars_list)
    RecyclerView recyclerView;

    @BindView(R.id.cars_view_show_map_button)
    Button showMapButton;

    @OnClick(R.id.cars_view_show_map_button)
    public void onShowMapButtonClicked() {
        carsPresenter.onShowMapButtonClicked();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerCarsComponent.builder()
                .carApplicationComponent(((CarApplication) getApplication()).getComponent())
                .carsModule(new CarsModule(this))
                .build().inject(this);

        setContentView(R.layout.cars_view);

        ButterKnife.bind(this);

        setupRecyclerView();

        carsPresenter.onCreate();
    }

    private void setupRecyclerView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(carsListAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        carsPresenter.onDestroy();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showList() {
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideList() {
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void showMapButton() {
        showMapButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void setupCarsList(List<Car> cars) {
        carsListAdapter.swapCars(cars);
    }

    @Override
    public void showErrorMsg(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMapActivity(List<Car> cars) {
        MapActivity.start(this, cars);
    }

    @Override
    public void hideMapButton() {
        showMapButton.setVisibility(View.GONE);
    }


}
