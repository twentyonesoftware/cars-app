package pl.software21.carapp.activity.cars.mvp;

import java.util.List;

import pl.software21.carapp.activity.cars.mvp.view.CarsView;
import pl.software21.carapp.app.network.model.Car;

/**
 * Created by RavB on 20.06.2017.
 */

public class CarsPresenter implements CarsModel.DownloadingListener {

    private CarsView carsView;
    private CarsModel carsModel;

    public CarsPresenter(CarsView carsView, CarsModel carsModel) {
        this.carsView = carsView;
        this.carsModel = carsModel;
    }

    public void onCreate() {
        carsView.hideList();
        carsView.hideMapButton();
        carsView.showProgress();

        carsModel.downloadCars(this);
    }

    public void onDestroy() {
        carsModel.cancelDownloadingCars();
    }

    public void onShowMapButtonClicked() {
        carsView.showMapActivity(carsModel.getCars());
    }

    @Override
    public void onSuccess(List<Car> carList) {
        carsView.hideProgress();
        carsView.showList();
        carsView.showMapButton();

        carsView.setupCarsList(carList);
    }

    @Override
    public void onFailure(String msg) {
        carsView.hideProgress();
        carsView.showErrorMsg(msg);
    }
}
