package pl.software21.carapp.activity.map.mvp;

/**
 * Created by RavB on 21.06.2017.
 */

public interface MapView {

    void showMarker(double latitude, double longitude, String title);

    void moveCamera(double latitude, double longitude, float zoom);

}
