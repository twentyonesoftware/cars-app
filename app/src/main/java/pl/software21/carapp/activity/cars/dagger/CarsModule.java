package pl.software21.carapp.activity.cars.dagger;

import com.squareup.picasso.Picasso;

import dagger.Module;
import dagger.Provides;
import pl.software21.carapp.activity.cars.CarsActivity;
import pl.software21.carapp.activity.cars.mvp.CarsModel;
import pl.software21.carapp.activity.cars.mvp.CarsPresenter;
import pl.software21.carapp.activity.cars.mvp.view.CarsListAdapter;
import pl.software21.carapp.app.network.RemoteInterface;

/**
 * Created by RavB on 20.06.2017.
 */

@Module
public class CarsModule {

    private final CarsActivity activity;

    public CarsModule(CarsActivity activity) {
        this.activity = activity;
    }

    @CarsScope
    @Provides
    public CarsModel carsModel(RemoteInterface remoteInterface) {
        return new CarsModel(remoteInterface);
    }

    @CarsScope
    @Provides
    public CarsPresenter carsPresenter(CarsModel model) {
        return new CarsPresenter(activity, model);
    }

    @CarsScope
    @Provides
    public CarsListAdapter carsListAdapter(Picasso picasso) {
        return new CarsListAdapter(picasso);
    }

}
