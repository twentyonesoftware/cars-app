package pl.software21.carapp.activity.cars.mvp.view;

import java.util.List;

import pl.software21.carapp.app.network.model.Car;

/**
 * Created by RavB on 20.06.2017.
 */

public interface CarsView {

    void showProgress();

    void hideProgress();

    void showList();

    void hideList();
    
    void hideMapButton();
    
    void showMapButton();

    void setupCarsList(List<Car> cars);

    void showErrorMsg(String msg);

    void showMapActivity(List<Car> cars);

}
