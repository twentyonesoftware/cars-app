package pl.software21.carapp.activity.map.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by RavB on 21.06.2017.
 */

@Scope
@Retention(RetentionPolicy.CLASS)
public @interface MapScope {
}
