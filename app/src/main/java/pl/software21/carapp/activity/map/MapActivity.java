package pl.software21.carapp.activity.map;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pl.software21.carapp.R;
import pl.software21.carapp.activity.map.dagger.DaggerMapComponent;
import pl.software21.carapp.activity.map.dagger.MapModule;
import pl.software21.carapp.activity.map.mvp.MapPresenter;
import pl.software21.carapp.activity.map.mvp.MapView;
import pl.software21.carapp.app.CarApplication;
import pl.software21.carapp.app.network.model.Car;

/**
 * Created by RavB on 21.06.2017.
 */

public class MapActivity extends FragmentActivity implements OnMapReadyCallback, MapView {

    public static final String CAR_LIST_EXTRA = "CAR_LIST_EXTRA";

    @Inject
    MapPresenter presenter;

    private GoogleMap map;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerMapComponent.builder()
                .carApplicationComponent(((CarApplication) getApplication()).getComponent())
                .mapModule(new MapModule(this))
                .build().inject(this);

        setContentView(R.layout.map_view);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        presenter.onCreate(getIntent());
    }


    public static void start(Context context, List<Car> cars) {
        Intent intent = new Intent(context, MapActivity.class);
        intent.putExtra(CAR_LIST_EXTRA, new ArrayList<>(cars));
        context.startActivity(intent);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        presenter.onMapReady();
    }

    @Override
    public void showMarker(double latitude, double longitude, String title) {
        LatLng latLng = new LatLng(latitude, longitude);
        map.addMarker(new MarkerOptions().position(latLng).title(title));
    }

    @Override
    public void moveCamera(double latitude, double longitude, float zoom) {
        map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude)));
        map.moveCamera(CameraUpdateFactory.zoomTo(zoom));
    }
}
