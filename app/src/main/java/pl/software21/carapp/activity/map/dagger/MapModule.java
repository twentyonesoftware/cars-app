package pl.software21.carapp.activity.map.dagger;

import dagger.Module;
import dagger.Provides;
import pl.software21.carapp.activity.map.MapActivity;
import pl.software21.carapp.activity.map.mvp.MapModel;
import pl.software21.carapp.activity.map.mvp.MapPresenter;

/**
 * Created by RavB on 21.06.2017.
 */

@Module
public class MapModule {

    private MapActivity activity;

    public MapModule(MapActivity activity) {
        this.activity = activity;
    }

    @MapScope
    @Provides
    public MapPresenter mapPresenter(MapModel mapModel) {
        return new MapPresenter(activity, mapModel);
    }

    @MapScope
    @Provides
    public MapModel mapModel() {
        return new MapModel();
    }

}
